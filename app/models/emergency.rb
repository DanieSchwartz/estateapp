class Emergency < ApplicationRecord

  validates :alert, presence: true, length: { minimum: 4, maximum: 30 }
  validates :location, presence: true, length: { minimum: 2, maximum: 30 }
  validates :user, presence: true, length: { minimum: 2, maximum: 30 }
  validates :date, presence: true

  has_many :comments, dependent: :destroy
  validates :alert, presence: true, length: { minimum: 5 }
end
