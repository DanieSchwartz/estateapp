class CommentsController < ApplicationController
  def create
    @emergency = Emergency.find(params[:emergency_id])
    @comment = @emergency.comments.create(comment_params)
    redirect_to emergency_path(@emergency)
  end

  def destroy
    @emergency = Emergency.find(params[:emergency_id])
    @comment = @emergency.comments.find(params[:id])
    @comment.destroy
    redirect_to edit_emergency_path(@emergency)
  end

  private
  def comment_params
    params.require(:comment) .permit(:commenter, :body)
  end
end
