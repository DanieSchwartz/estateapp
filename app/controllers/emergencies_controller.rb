class EmergenciesController < ApplicationController
  def index
    @emergencies = Emergency.all
  end

  def show
    @emergency = Emergency.find(params[:id])
  end

  def new
    @emergency = Emergency.new
  end

  def edit
    @emergency = Emergency.find(params[:id])
  end

  def create
    @emergency = Emergency.new(emergency_params)

    if @emergency.save
      redirect_to @emergency
    else
      render 'new'
    end
  end

  def update
    @emergency = Emergency.find(params[:id])

    if @emergency.update(emergency_params)
      redirect_to @emergency
    else
      render 'edit'
    end
  end

  def destroy
    @emergency = Emergency.find(params[:id])
    @emergency.destroy

    redirect_to emergencies_path
  end

  private

  def emergency_params
    params.require(:emergency) .permit(:alert, :location, :user, :date)
  end
end
