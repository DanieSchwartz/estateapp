class CreateEmergencies < ActiveRecord::Migration[6.0]
  def change
    create_table :emergencies do |t|
      t.string :alert
      t.string :location
      t.string :user
      t.date :date

      t.timestamps
    end
  end
end
