Rails.application.routes.draw do
  devise_for :models
  get 'dashboard/index'
  resources :emergencies
  root 'dashboard#index'

  resources :emergencies do
    resources :comments
  end
end
